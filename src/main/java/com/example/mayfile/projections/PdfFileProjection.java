package com.example.mayfile.projections;

import org.springframework.data.rest.core.config.Projection;

import com.example.mayfile.entities.PdfFile;

@Projection(name = "simple", types = { PdfFile.class })
public interface PdfFileProjection {
    Long getId();
    String getFilename();
    String getHash();
}
