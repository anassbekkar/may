package com.example.mayfile.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfFileDto {
    private Long id;
    private String filename;
    private String hash;
    private long size;
}
