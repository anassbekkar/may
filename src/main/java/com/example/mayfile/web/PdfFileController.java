package com.example.mayfile.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.mayfile.entities.PdfFile;
import com.example.mayfile.services.PdfFileService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/pdf-files")
@Tag(name = "PDF File Management", description = "Endpoints for managing PDF files")
@RequiredArgsConstructor
public class PdfFileController {

    private final PdfFileService pdfFileService;

    @Operation(summary = "Upload a PDF file", description = "Upload a PDF file to the server")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "File uploaded successfully", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = PdfFile.class)) }),
        @ApiResponse(responseCode = "400", description = "Invalid file or request", content = @Content),
        @ApiResponse(responseCode = "500", description = "Failed to upload file", content = @Content)
    })
    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestPart("file") MultipartFile file) {
        try {
            PdfFile uploadedFile = pdfFileService.uploadFile(file);
            return ResponseEntity.status(HttpStatus.CREATED).body(uploadedFile);
        } catch (IOException | NoSuchAlgorithmException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body("Failed to upload file: " + e.getMessage());
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                                 .body("Invalid file or request: " + e.getMessage());
        }
    }

    @Operation(summary = "Get a PDF file by ID", description = "Retrieve a PDF file by its ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "File found", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = PdfFile.class)) }),
        @ApiResponse(responseCode = "404", description = "File not found", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<?> getFileById(@PathVariable Long id) {
        try {
            PdfFile pdfFile = pdfFileService.getFileById(id)
                    .orElseThrow(() -> new RuntimeException("File not found with id: " + id));
            return ResponseEntity.ok(pdfFile);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                                 .body("File not found: " + e.getMessage());
        }
    }

    @Operation(summary = "Get all PDF files", description = "Retrieve a list of all PDF files")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of files", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = PdfFile.class)) })
    })
    @GetMapping
    public ResponseEntity<List<PdfFile>> getAllFiles() {
        List<PdfFile> files = pdfFileService.getAllFiles();
        return ResponseEntity.ok(files);
    }

    @Operation(summary = "Get duplicate PDF files", description = "Retrieve a list of duplicate PDF files based on their hash")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "List of duplicate files", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = PdfFile.class)) })
    })
    @GetMapping("/duplicates")
    public ResponseEntity<List<PdfFile>> getDuplicateFiles() {
        List<PdfFile> duplicates = pdfFileService.getDuplicateFiles();
        return ResponseEntity.ok(duplicates);
    }

    @Operation(summary = "Download a PDF file", description = "Download a PDF file by its ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "File downloaded", content = {
            @Content(mediaType = "application/octet-stream") }),
        @ApiResponse(responseCode = "404", description = "File not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Failed to download file", content = @Content)
    })
    @GetMapping("/download/{id}")
    public ResponseEntity<?> downloadFile(@PathVariable Long id) {
        try {
            byte[] fileData = pdfFileService.downloadFile(id);
            PdfFile pdfFile = pdfFileService.getFileById(id)
                    .orElseThrow(() -> new RuntimeException("File not found with id: " + id));
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + pdfFile.getFilename() + "\"");
            return ResponseEntity.ok().headers(headers).body(fileData);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body("Failed to download file: " + e.getMessage());
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                                 .body("File not found: " + e.getMessage());
        }
    }

    @Operation(summary = "View a PDF file", description = "View a PDF file by its ID")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "File viewed", content = {
            @Content(mediaType = "application/pdf") }),
        @ApiResponse(responseCode = "404", description = "File not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Failed to view file", content = @Content)
    })
    @GetMapping("/view/{id}")
    public ResponseEntity<Resource> viewPdf(@PathVariable Long id) {
        try {
            // Assuming the PDF files are stored in a directory named "pdfs"
            Path filePath = Paths.get("pdfs").resolve(id + ".pdf").normalize();
            Resource resource = new UrlResource(filePath.toUri());

            if (resource.exists()) {
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                        .body(resource);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (MalformedURLException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
