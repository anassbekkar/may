package com.example.mayfile.enums;

public enum PdfFileStatus {
    UPLOADED,
    PROCESSING,
    READY
}

