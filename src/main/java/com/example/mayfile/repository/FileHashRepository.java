package com.example.mayfile.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mayfile.entities.FileHash;

@Repository
public interface FileHashRepository extends JpaRepository<FileHash, Long> {
    Optional<FileHash> findByHash(String hash);
}

