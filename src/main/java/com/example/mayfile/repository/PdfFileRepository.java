package com.example.mayfile.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mayfile.entities.PdfFile;

@Repository
public interface PdfFileRepository extends JpaRepository<PdfFile, Long> {
    Optional<PdfFile> findByFilename(String filename);
    Optional<PdfFile> findByHash(String hash);
}


