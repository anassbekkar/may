package com.example.mayfile.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.mayfile.entities.PdfFile;
import com.example.mayfile.repository.PdfFileRepository;

import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PdfFileServiceImpl implements PdfFileService {

    private final PdfFileRepository pdfFileRepository;
    
    @Value("${file.upload-dir}")
    private String rootLocation;

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(Paths.get(rootLocation));
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage location", e);
        }
    }

    @Override
    @Transactional
    public PdfFile uploadFile(MultipartFile file) throws IOException, NoSuchAlgorithmException {
        String filename = file.getOriginalFilename();
        if (filename == null) {
            throw new RuntimeException("Invalid file name.");
        }

        Path destinationFile = Paths.get(rootLocation).resolve(filename).normalize().toAbsolutePath();

        if (!destinationFile.getParent().equals(Paths.get(rootLocation).toAbsolutePath())) {
            throw new RuntimeException("Cannot store file outside current directory.");
        }

        try (var inputStream = file.getInputStream()) {
            Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
        }

        String fileHash = hashFile(destinationFile);
        long fileSize = Files.size(destinationFile);

        Optional<PdfFile> existingFile = pdfFileRepository.findByHash(fileHash);
        if (existingFile.isPresent()) {
            throw new RuntimeException("Duplicate file detected.");
        }

        PdfFile pdfFile = new PdfFile(null, filename, destinationFile.toString(), fileHash, fileSize, null);
        return pdfFileRepository.save(pdfFile);
    }

    @Override
    public Optional<PdfFile> getFileById(Long id) {
        return pdfFileRepository.findById(id);
    }

    @Override
    public List<PdfFile> getAllFiles() {
        return pdfFileRepository.findAll();
    }

    @Override
    public List<PdfFile> getDuplicateFiles() {
        List<PdfFile> allFiles = pdfFileRepository.findAll();
        return allFiles.stream()
                .collect(Collectors.groupingBy(PdfFile::getHash))
                .values().stream()
                .filter(list -> list.size() > 1)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<PdfFile> getFileByFilename(String filename) {
        return pdfFileRepository.findByFilename(filename);
    }

    @Override
    public byte[] downloadFile(Long id) throws IOException {
        Optional<PdfFile> pdfFileOpt = getFileById(id);
        if (pdfFileOpt.isEmpty()) {
            throw new RuntimeException("File not found.");
        }
        Path file = Paths.get(pdfFileOpt.get().getPath());
        return Files.readAllBytes(file);
    }

    @Override
    public byte[] viewFile(Long id) throws IOException {
        return downloadFile(id);  // Reusing the downloadFile method to get the file data
    }

    private String hashFile(Path file) throws IOException, NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] fileBytes = Files.readAllBytes(file);
        byte[] hashBytes = digest.digest(fileBytes);
        StringBuilder hexString = new StringBuilder();
        for (byte b : hashBytes) {
            hexString.append(String.format("%02x", b));
        }
        return hexString.toString();
    }
}
