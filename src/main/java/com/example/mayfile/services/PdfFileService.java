package com.example.mayfile.services;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

import com.example.mayfile.entities.PdfFile;

public interface PdfFileService {
    PdfFile uploadFile(MultipartFile file) throws IOException, NoSuchAlgorithmException;
    Optional<PdfFile> getFileById(Long id);
    List<PdfFile> getAllFiles();
    List<PdfFile> getDuplicateFiles();
    Optional<PdfFile> getFileByFilename(String filename);
    byte[] downloadFile(Long id) throws IOException;
    byte[] viewFile(Long id) throws IOException;  // New method declaration
}
