package com.example.mayfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class MayfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(MayfileApplication.class, args);
    }

    // OpenAPI Configuration
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().info(new Info().title("Mayfile API").version("1.0.0"));
    }

    // Swagger UI Configuration
    @Bean
    public WebMvcConfigurer swaggerUiConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/swagger-ui/**").addResourceLocations("classpath:/META-INF/resources/webjars/springdoc-openapi-ui/");
            }
        };
    }
}
